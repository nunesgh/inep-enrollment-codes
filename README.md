# INEP [^inep] Enrollment Codes

Students enrollment codes selected for privacy vulnerability analyses.

DOI: [10.5281/zenodo.6533675](https://doi.org/10.5281/zenodo.6533675).

We randomly selected only one record for each student with a same unique pseudonymization code (`ID_ALUNO`) in each dataset. This repository contains the enrollment code (`ID_MATRICULA`) for each selected record.

The resulting datasets were used for vulnerability assessment using the [BVM library](https://github.com/nunesgh/bvm-library) ([10.5281/zenodo.6533704](https://doi.org/10.5281/zenodo.6533704)). The assessment results were published in:
- Gabriel H. Nunes - _A formal quantitative study of privacy in the publication of official educational censuses in Brazil_ (2021, [hdl:1843/38085](https://doi.org/hdl:1843/38085)).
- Mário S. Alvim, Natasha Fernandes, Annabelle McIver, Carroll Morgan, Gabriel H. Nunes - _Flexible and scalable privacy assessment for very large datasets, with an application to official governmental microdata_ (2022, [10.48550/arXiv.2204.13734](https://doi.org/10.48550/arXiv.2204.13734)). For this publication, also refer to [10.5281/zenodo.6533684](https://doi.org/10.5281/zenodo.6533684) ([github.com/nunesgh/inep-anonymization](https://github.com/nunesgh/inep-anonymization)).

## License

[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/) [^compatibility].

---

[^inep]:
    The [Anísio Teixeira National Institute of Educational Studies and Research](https://www.gov.br/INEP).
[^compatibility]:
    To understand how the various GNU licenses are compatible with each other, please refer to:
    
    https://www.gnu.org/licenses/gpl-faq.html#AllCompatibility

